# Open Chemistry Lab

This project was part of the course "Chemistry in Cyberspace" at JLU Giessen.
The goal was to implement the basic functionalities of chemical lab equipment in an educational game.
Furthermore, the project is under a permissive Creative Commons license and we include a list of resources for those who would like to get started with educational game development.

The contributors are

Paul Debes

Michael Kirchner

Mark Melvin Pradja

Marcel Ruth

Moritz Schäfer


## Resources


### Unreal

Offical Unreal Engine online learning portal - https://learn.unrealengine.com/

Games with Katie - https://www.youtube.com/channel/UCqI-7BueefkQwfbOi8ZzMOw


### Blender

https://www.youtube.com/watch?v=0l9ZUD15A9c

Blender Links:
Reduce the polygons count:
https://help.augment.com/en/articles/2666702-blender-how-to-reduce-the-polygons-count

Videos Blender:
Material:
https://www.youtube.com/watch?v=Bg90pIB-IP0&t=50s

Model a pipe:
https://www.youtube.com/watch?v=AMXcQ2V-VGA&t=22s

Building a low poly bottle:
https://www.youtube.com/watch?v=mQL0hrvZl7I

Glass in Blender:
https://www.youtube.com/watch?v=8OQuwodQySQ&t=72s

Holes in Surfaces in Blender:
https://www.youtube.com/watch?v=Ci1jBOm_5NY

LowPoly Bottles:
https://www.youtube.com/watch?v=4hPDrzHrOhk
https://www.youtube.com/watch?v=5S7rnkd1tGc&t=8s

Blender Guru Donut Tutorial:
https://www.youtube.com/watch?v=TPrnSACiTJ4&list=PLjEaoINr3zgEq0u2MzVgAaHEBt--xLB6U


Videos Unreal Engine 4:

Scaling Objects:
https://www.youtube.com/watch?v=VuyF6JKpeRc&t=75s

Glass in Unreal:
https://www.youtube.com/watch?v=-9GXeIWjDLM

Building and Exporting Blender Materials:
https://www.youtube.com/watch?v=dWY0QCj0ZzI&t=160s
https://www.youtube.com/watch?v=MDyMV76IiqA&t=40s

Export Models from Blender to unreal:
https://www.youtube.com/watch?v=Wr8qJheu69M&t=309s




We do not claim to own any of the StarterContent provided by epic and still included in the project files.

